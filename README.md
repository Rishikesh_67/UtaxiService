# UtaxiService

A repository containing the source code for a Cab Booking web application. 

The main intension for the web app is to provide a help to existing http://utaxi.in/Home.aspx to make its market grow easily.

### Frontend 

| Technologies |
| ------------ |
| HTML |
| CSS3 |
| JavaScript |
| AngularJS |

### Backend

| Technologies |
| ------------ |
| Python/Django |

### Database used

| Technologies |
| ------------ |
| MySQL 5.7.18 |


![Cab](http://www.roiinvesting.com/wp-content/uploads/2016/06/Taxi-Cab-2.jpg)
